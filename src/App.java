import java.util.Scanner;
import java.lang.Math;

public class App {
    public static void main(String[] args){
        calcularHipotenusa();
    }

    public static void conversion_Kmh_to_ms(){
        Scanner entrada = new Scanner(System.in);

        double numero;
        double nuevaVelocidad;

        System.out.print("Ingrese velocidad en Km/h para convertir a m/s: ");
        numero = entrada.nextDouble();

        nuevaVelocidad = numero * (10.0/36.0);

        System.out.print(numero +" km/h es igual a "+nuevaVelocidad+" m/s .");
        entrada.close();
    }

    public static void calcularHipotenusa () {
        Scanner entradaCatetos = new Scanner(System.in);
        
        double cateto1 = 0;
        double cateto2 = 0;

        System.out.print("Ingresar medida cateto adyacente: ");
        cateto1 = entradaCatetos.nextDouble();
        System.out.print("Ingresar medida cateto opuesto: ");
        cateto2 = entradaCatetos.nextDouble();

        System.out.print("Medida hipotenusa: "+ Math.sqrt(cateto1*cateto1 + cateto2*cateto2) );
        entradaCatetos.close();
        }
}
